<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Database\QueryException;

trait ApiHelper
{
    protected function onSuccess(mixed $data): JsonResponse
    {
        return response()->json([
            'status' => 'OK',
            'data' => $data,
        ]);
    }

    protected function onError(int $code, string $message = '', \Throwable $e = null): JsonResponse
    {
        if (!empty($e)) {
            switch (get_class($e)) {
                case QueryException::class:
                    $code = 500;
                    $message = 'Terjadi kesalahan pada aplikasi, harap hubungi admin';
                    break;
    
                default:
                    break;
            }
        }

        return $this->sendResponse($code, $message);
    }

    protected function sendResponse(int $code, string $message): JsonResponse
    {
        return response()->json([
            'code' => $code,
            'message' => $message,
        ], $code);
    }
}
