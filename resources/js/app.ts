import './bootstrap';

import { createApp } from 'vue';
import { createHead } from '@unhead/vue'

import App from './components/App.vue'
import router from './router';

const app = createApp(App)
const head = createHead()

app.use(router)
app.use(head)
app.mount('#app')