# Laravel Vue Boilerplate

## TechStack
- Laravel 11
- PHP 8.2
- Vue 3 + Vite
- Typescript
- TailwindCSS 3

## Getting Started
Minimal Setup

1. Install dependency for laravel  
```
composer install
```

2. Install dependency for vue  
```
# npm
npm install

# pnpm
pnpm install
```

3. Set the application key  
```
php artisan key:generate
```

4. Run vue application and watch for changes  
```
# npm
npm run dev

# pnpm
pnpm dev
```

5. Run laravel application and watch for changes  
```
php artisan serve
```