<?php

use Illuminate\Support\Facades\Route;

/**
 * Creating web route that accept all route for SPA Vue, Excluding route with /api prefix
 */
Route::get('/{pathMatch?}', function () {
    return view('app');
})->where('pathMatch', '^(?!api).*$');
